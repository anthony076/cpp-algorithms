
#include <iostream>
#include <cmath>
#include <iomanip>
#include "common.h"

#define IGNORE

using namespace std;

int main(int argc, const char * argv[])
{

#ifndef IGNORE
    simple_average();
    continus_sum();
#endif

}

void simple_average(){

    cout << "==== 輸出平均數，指定小數位數 ===="  << endl;

    int a, b, c;

    cout << "Input random number:\n" << endl;
    cin >> a >> b >> c;

    double avg = (a+b+c)/3.0;

    // setprecision(小數點總位數)
    cout << setprecision(4) << avg << endl;
}

void continus_sum(){

    // Sum of 1+2+3+.....+n

    cout << "==== 連續求和 ===="  << endl;

    int n;

    cout << "Input a number:\n" << endl;
    cin >> n ;

    int sum = (1+n) * n/2;
    cout << "總和為 : " << sum << endl;

}
