#include <iostream>
#include "select.h"

// 計算 array size
#define getSize(arr) *(&arr+1)-arr

using namespace std;

// 列印 array 內容
void print(int* arr, unsigned long len){

    for (int i=0; i<len; i++){
        cout << arr[i] << " ";
    }

    cout << endl;
}

int main(int argc, const char * argv[])
{
    int number[3] = {2,3,1};
    unsigned long size_number = getSize(number);

    selectSort(number,size_number,ascending);
    print(number,size_number);

}


