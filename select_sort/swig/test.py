import cmodule

print(cmodule.ascending(1, 2))

print(cmodule.ascending(5566, 2))


print(cmodule.ASCENDING)

# python 的 list 和 c++ 中的 array 是不一樣的東西
# https://stackoverflow.com/questions/36222455/swigcpython-passing-and-receiving-c-arrays
print(cmodule.selectedIdx([1, 2, 3], 0, 3, cmodule.ASCENDING))

