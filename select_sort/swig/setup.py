

import setuptools

from distutils.core import setup, Extension

external_module = Extension('_cmodule', sources=['cmodule_wrap.cxx', '../src/select.cpp'])

setup(
    name = 'cmodule',
    version = '0.1',
    author = 'ching',
    description = """ cmodule""",
    ext_modules = [external_module],
    py_modules = ["cmodule"]
)