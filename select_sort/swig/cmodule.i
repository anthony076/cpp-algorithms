%module cmodule

/* 載入標頭檔 */
%{
#include "../src/select.h"
%}


/* 函數宣告，有宣告才會產生對應的函數 */
int ascending(int a, int b);
int selectedIdx(int* arr, int from, int to, int(*compar)(int, int));
void selectSort(int* arr, int len, int(*compar)(int,int));

/*
 %constant 讓 python 可以使用函數指針
 ASCENDING 為 指向ascending函數的指針，提供給python 使用
 */
%constant int (*ASCENDING)(int,int) = ascending;


