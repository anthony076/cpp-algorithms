#!/usr/bin/env bash

# generate *.py and *_wrap.cxx
swig -c++ -python cmodule.i

# generate *.pyd
python3 setup.py build_ext --inplace

# change _cmodule.cpython-36m-darwin.so to _cmodule.so
mv _cmodule.cpython-36m-darwin.so _cmodule.so