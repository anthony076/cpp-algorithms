import pytest
from typing import Callable
from typing import List

def swap(arr: List[int], aIdx: int, bIdx: int) -> List[int]:
    """ swap two elements in a list """
    arr[bIdx], arr[aIdx] = arr[aIdx], arr[bIdx]
    return arr

def compare_ascending(a: int, b: int) -> int:
    """ compare two numbers by subtraction """
    return a - b

def selectIndex(arr: List[int], fromIdx: int, toIdx: int, compareFn: Callable[[int, int], int]) -> int:
    """
    compare elements in a list, and return the index of the element
    which has a minimum value
    """
    selected = fromIdx

    for i in range(fromIdx+1, toIdx, 1):

        if compareFn(arr[i], arr[selected]) < 0:
            selected = i

    return selected

def selectSort(arr: List[int], swapMethod: Callable[[int, int], int]) -> List[int]:
    length = len(arr)

    for i in range(0, length, 1):
        selected = selectIndex(arr, i, length, swapMethod)
        if selected != i:
            swap(arr, i, selected)

    return arr

@pytest.mark.parametrize("arr, aIdx, bIdx, expect", [
    ([1, 2, 3], 0, 2, [3, 2, 1]),
    ([1, 2, 3], 1, 2, [1, 3, 2]),
    ([1, 2, 3], 0, 1, [2, 1, 3]),
])
def test_swap(arr: List[int], aIdx, bIdx, expect):
    assert swap(arr, aIdx, bIdx == expect)

@pytest.mark.parametrize("a, b, expect", [
    (3, 5, -2),
    (5, 3, 2)
])
def test_compare_ascending(a, b, expect):
    assert (compare_ascending(a, b) == expect)

@pytest.mark.parametrize("arr, fromIdx, toIdx, expect", [
                             ([10, 1], 0, 2, 1),
                             ([1, 10], 0, 2, 0),
                             ([1, 2, 3], 0, 3, 0),
                             ((3, 2, 1), 0, 3, 2),
                             ((2, 1, 3), 0, 3, 1)
                         ])
def test_selectIndex(arr: List[int], fromIdx, toIdx, expect):
    assert (selectIndex(arr, fromIdx, toIdx, compare_ascending) == expect)

@pytest.mark.parametrize("arr, expect", [
    ([3, 2, 1], [1, 2, 3]),
    ([10, 9, 1, 2, 5, 3, 8, 7], [1, 2, 3, 5, 7, 8, 9, 10])
])
def test_selectSort(arr: List[int], expect):
    assert (selectSort(arr, compare_ascending) == expect)

def main():
    numbers = [10, 9, 1, 2, 5, 3, 8, 7]
    selectSort(numbers, compare_ascending)
    print(numbers)

if __name__ == "__main__":
    main()
