
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "select.h"
#include <vector>
#include <iostream>
#define getSize(arr) *(&arr+1)-arr

// 用來比較兩個array的內容是否相同
// 因為函數只能傳遞 array指針，不能傳遞本身，所以需要指定長度
bool is_same(int* arr1, int* arr2, int size1, int size2){
    if (size1 != size2) {
        return false;
    }

    for(int i=0;i<size1;i++){
        if (arr1[i] != arr2[i]){
            return false;
        }
    }
    return true;
}

TEST_CASE("test_ascending"){
    CHECK( ascending(5,1) == 4);
    REQUIRE_FALSE(ascending(5,1)!=4);
}

TEST_CASE("test_selectedIdx"){
    // vector 初始化，建立空間大小為 3個 int 大小的 vector
    // 注意，vector 要初始化後才能使用
    std::vector<int> vec(3);
    // 或用 std::vector<int> vec ={ 1,2,3 };

    int* uut = &vec[0];

    SECTION("normal case"){
        vec = {1, 2, 3};
        CHECK( selectedIdx(uut, 0, 3, ascending) == 0);
    }

    SECTION("fail case"){
        vec = {2, 1, 3};
        REQUIRE_FALSE( selectedIdx(uut, 0, 3, ascending) == 3);
    }

    SECTION("duplicated number"){
        vec = {1, 1, 2};
        CHECK( selectedIdx(uut, 0, 3, ascending) == 0);
    }

}

TEST_CASE("test_selectSort"){
    int uut[] = {10,1};
    int mock[] = {1,10};

    int size_uut = static_cast<int>(getSize(uut));
    int size_mock = static_cast<int>(getSize(mock));
    selectSort(uut,size_uut,ascending);

    //CHECK(compare(uut, mock, size_uut, size_mock) == true);
    CHECK(is_same(uut, mock, size_uut, size_mock));
}